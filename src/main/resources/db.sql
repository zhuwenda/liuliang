use ll;

CREATE TABLE IF NOT EXISTS t_order
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT '主键,序列自增',
  orderid VARCHAR(50) COMMENT '订单编号',
  fee_amount INT COMMENT '支付价格',
  num INT COMMENT '购买数量',
  productid VARCHAR(40) COMMENT '产品ID',
  pay_status INT COMMENT '支付状态，1:支付成功',
  topup_status INT COMMENT '充值状态 5:充值成功',
  topup_mobile VARCHAR(20) COMMENT '充值流量的手机号码',
  topup_orderid VARCHAR(100) COMMENT '流量平台生成的订单号',
  payid VARCHAR(40) COMMENT '微信支付流水号',
  create_time DATETIME NOT NULL COMMENT '下单时间',
  openid VARCHAR(50) NOT NULL COMMENT '微信用户的openid',
  ip VARCHAR(20) COMMENT '用户下单时的IP地址',
  source_openid VARCHAR(40) COMMENT '来源(openid)',
  outer_message VARCHAR(300) COMMENT '外部接口返回消息',
  channel_type VARCHAR(30) COMMENT '渠道',
  channel_id VARCHAR(64) COMMENT '渠道id',
  refund_no VARCHAR(64) COMMENT '内部退款单号',
  refund_id VARCHAR(64) COMMENT '微信退款ID'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '订单表';



CREATE TABLE IF NOT EXISTS t_product
(
  productid VARCHAR(40) PRIMARY KEY NOT NULL COMMENT '主键,产品ID',
  price INT COMMENT '产品普通售价',
  vip_price INT COMMENT 'VIP用户的产品售价',
  name VARCHAR(50) NOT NULL COMMENT '产品名称',
  on_sale int NOT NULL COMMENT '是否上架. 1:上架',
  carrier int COMMENT '（特别的，当全运营商适用时，设置为null值）适用运营商(1.中国移动，2.中国联通，3.中国电信',
  province VARCHAR(20) COMMENT '（特别的，当全国适用时，设置为null值）适用省份（广东，湖南，湖北。。。）',
  outer_productid VARCHAR(50) COMMENT '外部接口产品ID',
  outer_extend1 VARCHAR(50) COMMENT '外部接口所需扩展字段1',
  interface_flag VARCHAR(50) COMMENT '实现接口标识',
  order_num int COMMENT '排序号（值越大优先级越高）',
  is_auto_down int COMMENT '是否自动下架（1为自动下架）'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '产品表';

CREATE TABLE IF NOT EXISTS t_bill
(
  flowid VARCHAR(64) PRIMARY KEY NOT NULL COMMENT '流水号',
  userid VARCHAR(64) NOT NULL COMMENT '用途ID',
  fund_flow_type VARCHAR(30) NOT NULL COMMENT '资金流水类型（收入、支出）',
  trade_type VARCHAR(30) NOT NULL COMMENT '交易类型',
  amount INT NOT NULL COMMENT '金额',
  operator VARCHAR(64) NOT NULL COMMENT '操作人',
  operate_time DATETIME NOT NULL COMMENT '操作时间',
  orderid VARCHAR(50) COMMENT '订单号',
  describption VARCHAR(100) COMMENT '描述'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '账单表';

CREATE TABLE IF NOT EXISTS t_channel_user
(
  userid VARCHAR(64) PRIMARY KEY NOT NULL COMMENT '用户ID',
  channel_type VARCHAR(30) NOT NULL COMMENT '渠道类型',
  create_time DATETIME NOT NULL COMMENT '创建时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '渠道用户表';

CREATE TABLE IF NOT EXISTS t_channel_distribution_product
(
  userid VARCHAR(40) NOT NULL COMMENT '所属用户ID',
  productid VARCHAR(40) COMMENT '关联的产品ID',
  channel_dist_price INT COMMENT '产品普通售价',
  PRIMARY KEY(userid,productid)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '渠道分销产品表';

CREATE TABLE IF NOT EXISTS t_channel_product
(
  channel_type VARCHAR(30) NOT NULL COMMENT '渠道',
  productid VARCHAR(40) COMMENT '关联的产品ID',
  channel_price INT COMMENT '渠道价格',
  on_sale int NOT NULL COMMENT '是否上架. 1:上架',
  PRIMARY KEY(channel_type,productid)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '渠道产品表';

CREATE TABLE IF NOT EXISTS t_vip
(
  openid VARCHAR(50) PRIMARY KEY NOT NULL COMMENT '微信openid'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'vip用户表';


CREATE TABLE t_wxuser
(
  openid VARCHAR(40) PRIMARY KEY COMMENT '用户openid',
  nickname VARCHAR(40) COMMENT '微信用户昵称',
  headimgurl VARCHAR(200) COMMENT '微信用户头像',
  subscribe INT COMMENT '是否已关注。1已关注，否则未关注',
  recommender VARCHAR(40) COMMENT '推荐人的openid',
  subscribe_time DATETIME COMMENT '关注时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT '微信用户表';

-- 查询用户的推荐人，level1 level2 level3 分别代表 一级推荐人、二级推荐人、三级推荐人
select t2.openid as level1, t3.openid as level2, t4.openid as level3
from t_wxuser_old_backup t1
  LEFT JOIN t_wxuser_old_backup t2 on t1.recommender = t2.openid
  LEFT JOIN t_wxuser_old_backup t3 on t2.recommender = t3.openid
  LEFT JOIN t_wxuser_old_backup t4 on t3.recommender = t4.openid
where t1.openid=4;


CREATE TABLE t_wxuser_share_code
(
  openid VARCHAR(40) COMMENT '用户openid',
  share_code VARCHAR(40) UNIQUE KEY COMMENT '分享者ID，与openid是一一对应的关系。',
  share_type INT(4) NOT NULL UNIQUE KEY COMMENT '分享类型。值域[1000,9999]'
)ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT '微信用户分享码关联表';


CREATE TABLE t_parameter_qrcode
(
  sceneid VARCHAR(64) PRIMARY KEY COMMENT 'scene id 场景值',
  ticket VARCHAR(128) UNIQUE KEY COMMENT '获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。',
  expire_seconds INT COMMENT '该二维码有效时间，以秒为单位',
  url VARCHAR(128) COMMENT '二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片',
  last_update_time DATETIME COMMENT '最后更新时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT '参数二维码表';



CREATE TABLE t_parameter
(
  param_key VARCHAR(50) NOT NULL PRIMARY KEY  COMMENT '参数键名',
  param_value VARCHAR(200) COMMENT '参数值',
  param_name VARCHAR(50) COMMENT '参数名',
  param_desc VARCHAR(200) COMMENT '参数描述'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '参数表';

CREATE TABLE t_product_commission
(
  productid VARCHAR(50) NOT NULL PRIMARY KEY COMMENT '产品ID',
  level1 int COMMENT '一级佣金。百分值，例如千分之3的佣金比例，则值为 3',
  level2 int COMMENT '二级佣金。百分值',
  level3 int COMMENT '三级佣金。百分值',
  source_openid int COMMENT 'openid来源的佣金。百分值'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '产品佣金规则表';

CREATE TABLE t_user_commission
(
  openid VARCHAR(50) NOT NULL COMMENT '获得佣金的用户的openid',
  grade VARCHAR(20) COMMENT '佣金分成级别，一级、二级、三级',
  amount int COMMENT '所得佣金。单位：分',
  orderid VARCHAR(50) COMMENT '订单编号',
  create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '用户佣金记录表';

CREATE TABLE t_withdraw
(
  withdraw_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT '主键,序列自增',
  openid VARCHAR(50) NOT NULL COMMENT '申请提现用户的openid',
  amount int COMMENT '提现金额',
  account VARCHAR(50) COMMENT '提现账户（支付宝）',
  status int COMMENT '提现状态：1：提现中、5：提现成功、9：提现失败',
  note VARCHAR(50) COMMENT '用户备注',
  status_msg VARCHAR(50) COMMENT '提现状态消息',
  create_time DATETIME COMMENT '提现时间',
  payment_no VARCHAR(64) COMMENT '微信订单号'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '提现表';
