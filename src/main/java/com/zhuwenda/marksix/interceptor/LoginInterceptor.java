package com.zhuwenda.marksix.interceptor;


import com.zhuwenda.marksix.annotation.Anonymous;
import com.zhuwenda.marksix.controller.WechatController;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.WxService;
import com.zhuwenda.marksix.service.WxuserService;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    private String[] whitelist;
    @Value("${isTest}")
    private String isTest;

    public LoginInterceptor(){
        whitelist = new String[]{"/hotdata/callback","/dahan/callback","/jingnan/callback"
                ,"/pay/wx/callback","/share","/callback"};
    }

    @Autowired
    private WxService wxService;
    @Autowired
    private WxuserService wxuserService;

    @Value("${domain}")
    private String domain;
    @Value("${weixin.manager.openids}")
    private String managerOpenids;


    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object obj) throws Exception {
        HttpSession session = request.getSession();
        String code = request.getParameter("code");

        String url = "http://" + domain + (request.getQueryString() != null ? request.getRequestURI() + "?" + request.getQueryString() : request.getRequestURI());

        String originalUrl = request.getRequestURL().toString();
        if("1".equals(isTest)){
            if(managerOpenids != null && !"".equals(managerOpenids.trim())){
                session.setAttribute("openid",managerOpenids.split(",")[0]);
                session.setAttribute("isVip",true);
                session.setAttribute("isManager",true);
                return true;
            }
        }
        String openid = (String)session.getAttribute("openid");

        for(int i = 0; i < whitelist.length; i++){
            if(request.getRequestURI().toString().startsWith(whitelist[i])){
                return true;
            }
        }
        if(obj instanceof HandlerMethod){
            if(((HandlerMethod)obj).getBeanType().getAnnotationsByType(Anonymous.class).length > 0){
                return true;
            }else if(((HandlerMethod)obj).getMethod().getDeclaredAnnotationsByType(Anonymous.class).length > 0){
                return true;
            }
        }

        if(openid == null){//未登录
            if(code != null && !"".equals(code)){//携带了登录认证信息
                WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxService.oauth2getAccessToken(code);

                if (wxMpOAuth2AccessToken.getOpenId() != null){
                    session.setAttribute("openid", wxMpOAuth2AccessToken.getOpenId());
                    session.setAttribute("isVip", wxuserService.isVip((String) request.getSession().getAttribute("openid")));
                    session.setAttribute("isManager",isManager(wxMpOAuth2AccessToken.getOpenId()));
                    return true;
                }else {
                    return false;
                }
            }else{
                String authUrl = wxService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_BASE, null);
                response.sendRedirect(authUrl);
                return false;
            }
        }

        return true;
    }

    /**
     * 判断是否为管理员
     * @param openid 微信用户的openid
     * @return
     */
    private boolean isManager(String openid){
        if(openid != null && managerOpenids != null){
            String[] t = managerOpenids.split(",");
            for(int i = 0; i < t.length; i++){
                if(t[i].equals(openid)){
                    return true;
                }
            }
        }
        return false;
    }
}