package com.zhuwenda.marksix.controller;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.service.UserCommissionService;
import com.zhuwenda.marksix.service.WxTemplateMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value="/hotdata")
public class HotDataCallbackController {
    private static final Logger logger = LoggerFactory.getLogger(HotDataCallbackController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/callback",method = RequestMethod.POST)
    @ResponseBody
    public Map callback(String transactionID,String mobile,String product_id,String status,String message){
        Map ret = new HashMap<>();
        ret.put("msg", "1");

        logger.info("hotdata流量平台回调："+transactionID);

        if(transactionID == null){
            logger.info("transactionID is empty");
        }else {
            logger.info("transactionID:{},mobile:{},product_id:{},status:{},message:{}"
                    ,transactionID,mobile,product_id,status,message);



            Order order = orderService.queryOrderByOuterOrderid(transactionID);

            if(order != null){
                if(order.getTopupStatus() == Constant.ORDER_TOPUP_STATUS_TOPUPING){
                    Product product = productService.queryByProductid(order.getProductid());
                    if ("1".equals(status)) {
                        orderService.orderSuccessNotification(order, product);
                    } else {
                        logger.warn("hotdata平台返回充值结果失败，外部订单号:{},错误消息:{}", transactionID, message);
                        orderService.orderFailedNotification(order, product, message, TopupFailType.NORMAL);
                    }
                }else {
                    logger.info("订单充值状态不为充值中，不再处理");
                }
            }else{
                logger.error("找不到订单");
            }
        }

        return ret;
    }


}
