package com.zhuwenda.marksix.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.dahan.DaHanCallback;
import com.zhuwenda.marksix.bean.dahan.DaHanCbResponse;
import com.zhuwenda.marksix.service.*;
import com.zhuwenda.marksix.util.ObjectMapperFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletInputStream;

@Controller
@RequestMapping("/dahan")
public class DaHanCallbackController {

    private static final Logger logger = LoggerFactory.getLogger(DaHanCallbackController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/callback",method = {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public DaHanCbResponse callback(ServletInputStream inputStream){
        logger.info("大汉充值回调");
        DaHanCbResponse daHanCbResponse = new DaHanCbResponse();
        daHanCbResponse.setResultCode("1111");
        daHanCbResponse.setResultMsg("未知错误");

        try {
            DaHanCallback cbData = null;
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String cb = new String(bytes,"UTF-8");
            ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
            logger.info("回调原文：{}",cb);
            cbData = objectMapper.readValue(cb, DaHanCallback.class);

            if(cbData == null){
                logger.info("回调数据为空");
            }else {

                logger.info("大汉充值回调："+objectMapper.writeValueAsString(cbData));


                Order order = orderService.queryOrderByOuterOrderid(cbData.getClientOrderId());

                if(order != null){
                    if(order.getTopupStatus() == Constant.ORDER_TOPUP_STATUS_TOPUPING){

                        Product product = productService.queryByProductid(order.getProductid());
                        if(cbData.getStatus() != null && cbData.getStatus() == 0){
                            orderService.orderSuccessNotification(order,product);
                        }else {
                            logger.warn("大汉流量平台返回充值结果失败，外部订单号:{},错误消息:{}", cbData.getClientOrderId(), cbData.getErrorDesc());
                            orderService.orderFailedNotification(order, product, cbData.getErrorDesc(), TopupFailType.NORMAL);
                        }
                        daHanCbResponse.setResultCode("0000");
                        daHanCbResponse.setResultMsg("成功");

                    }else {

                        logger.info("订单充值状态不为充值中，不再处理");
                        daHanCbResponse.setResultCode("0000");
                        daHanCbResponse.setResultMsg("成功（状态不为充值中）");
                    }
                }else{
                    logger.error("找不到订单");
                    daHanCbResponse.setResultCode("1111");
                    daHanCbResponse.setResultMsg("找不到该订单");
                }
            }
        } catch (Exception e){
            logger.error("大汉充值回调异常",e);
            daHanCbResponse.setResultCode("1111");
            daHanCbResponse.setResultMsg(e.getMessage());
        }

        return daHanCbResponse;
    }

}
