package com.zhuwenda.marksix.bean;

import java.util.List;

/**
 * 分页支持对象
 * 封装了查询结果集和分页对象
 * @author 朱文达
 * @see Pager
 *
 */
public class PaginationSupport<T> {
	private List<T> list;
	private Pager pager;
	
	public PaginationSupport() {
	}

	public PaginationSupport(List list, Pager pager){
		this.list = list;
		this.pager = pager;
	}
	
	public List<T> getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public Pager getPager() {
		return pager;
	}
	public void setPager(Pager pager) {
		this.pager = pager;
	}
}
