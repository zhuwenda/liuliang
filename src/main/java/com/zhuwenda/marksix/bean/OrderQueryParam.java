package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.ChannelType;

import java.util.Date;

/**
 * 订单查询参数
 */
public class OrderQueryParam {

    private String orderid;
    private String openid;
    private Integer payStatus;
    private Integer notPayStatus;
    private Date beginDate;
    private Date endDate;
    private String keyword;
    private ChannelType channelType;
    private String channelId;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getNotPayStatus() {
        return notPayStatus;
    }

    public void setNotPayStatus(Integer notPayStatus) {
        this.notPayStatus = notPayStatus;
    }
}
