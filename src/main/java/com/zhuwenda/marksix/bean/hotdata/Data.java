package com.zhuwenda.marksix.bean.hotdata;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {

    @JsonProperty("transactionID")
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
