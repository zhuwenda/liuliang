package com.zhuwenda.marksix.bean;

/**
 * Created by wendazhu on 16/4/17.
 */
public class ChannelDistributionProduct {

    private String userid;
    private String productid;
    private Integer channelDistPrice;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public Integer getChannelDistPrice() {
        return channelDistPrice;
    }

    public void setChannelDistPrice(Integer channelDistPrice) {
        this.channelDistPrice = channelDistPrice;
    }
}
