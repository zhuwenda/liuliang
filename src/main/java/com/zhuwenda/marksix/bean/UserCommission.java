package com.zhuwenda.marksix.bean;


import java.sql.Date;

public class UserCommission {

    /** 一级会员 */
    public static final String GRADE_LEVEL1 = "level1";
    /** 二级会员 */
    public static final String GRADE_LEVEL2 = "level2";
    /** 三级会员 */
    public static final String GRADE_LEVEL3 = "level3";
    /** 来源（openid )*/
    public static final String GRADE_SOURCE_OPENID = "source_openid";

    private String openid;
    private String grade;
    private Integer amount;
    private String orderid;
    private Date createTime;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
