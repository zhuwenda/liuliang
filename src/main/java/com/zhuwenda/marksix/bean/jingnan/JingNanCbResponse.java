package com.zhuwenda.marksix.bean.jingnan;


public class JingNanCbResponse {

    private String code;
    private String message;
    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static JingNanCbResponse success(){
        return success(null);
    }

    public static JingNanCbResponse success(String message){
        JingNanCbResponse response = new JingNanCbResponse();
        response.setCode("0");
        response.setStatus("ok");
        response.setMessage(message == null ? "成功" : message);
        return response;
    }

    public static JingNanCbResponse businessError(){
        return businessError(null);
    }

    public static JingNanCbResponse businessError(String message){
        JingNanCbResponse response = new JingNanCbResponse();
        response.setCode("0");
        response.setStatus("error");
        response.setMessage(message == null ? "失败" : message);
        return response;
    }
    public static JingNanCbResponse systemError(){
        return systemError(null);
    }

    public static JingNanCbResponse systemError(String message){
        JingNanCbResponse response = new JingNanCbResponse();
        response.setCode("1");
        response.setStatus("error");
        response.setMessage(message == null ? "未知异常" : message);
        return response;
    }
}
