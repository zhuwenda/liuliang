package com.zhuwenda.marksix.bean.jingnan;


import com.fasterxml.jackson.annotation.JsonProperty;

public class JingNanChargeResult extends JingNanBaseResult {

    @JsonProperty("TaskID")
    private String taskid;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }
}
