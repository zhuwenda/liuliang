package com.zhuwenda.marksix.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by wendazhu on 15/11/7.
 */
public class MapParam {

    private Map<String,String> param;

    public MapParam(){
        param = new HashMap<String, String>();
    }

    public MapParam add(String name,String value){
        param.put(name,value);
        return this;
    }

    public String toJSON(){
        try{

            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(param);
            return json;
        } catch (Exception e){
            return "";
        }
    }

    public String toParamString(){
        StringBuffer sb = new StringBuffer();
        if(param.size() > 0){
            Iterator<Map.Entry<String,String>> it = param.entrySet().iterator();
            boolean isFirstLoop = true;
            while (it.hasNext()){
                Map.Entry<String,String> entry = it.next();
                if(isFirstLoop){
                    isFirstLoop = false;
                }else {
                    sb.append("&");
                }
                sb.append(entry.getKey()+"="+entry.getValue());
            }
        }
        return sb.toString();
    }

}
