package com.zhuwenda.marksix.util;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.IOException;


public class HttpUtil {

    public static void main(String[] str){

        String url = "http://api.hotdata.cc/api.php?m=Mobile&a=index";
        String data = "{\"data\":\"{\\\"method\\\":\\\"FlowRechargePPS\\\",\\\"openid\\\":\\\"86a8ad422e312f8604260564c7a4dfb3\\\",\\\"appkey\\\":\\\"33aabe74d2f21a0ba8c031c0d3498e1a\\\",\\\"mobile\\\":\\\"13074691245\\\",\\\"product_id\\\":\\\"1001\\\",\\\"product_value\\\":\\\"10\\\",\\\"timestamp\\\":\\\"20151220183000\\\",\\\"sign\\\":\\\"698d51a19d8a121ce581499d7b701668\\\"}\"}";
        String ret = HttpUtil.post(url,data);
        System.out.println(ret);
    }

    private static Logger logger = Logger.getLogger(HttpUtil.class);

    protected static CloseableHttpClient httpClient = HttpClients.createDefault();

    public static String get(String url){
        try {
            logger.info("send a HTTP GET request:"+url);
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            String resultContent = new BasicResponseHandler().handleResponse(response);
            return resultContent;
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String post(String url,String data){
        try {
            logger.info("send a HTTP POST request: "+url);
            logger.info("request data: "+data);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new StringEntity(data,"utf-8"));
            CloseableHttpResponse response = httpClient.execute(httpPost);

            String resultContent = new BasicResponseHandler().handleResponse(response);
            return resultContent;
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
