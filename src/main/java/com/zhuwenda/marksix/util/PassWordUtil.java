package com.zhuwenda.marksix.util;

import com.zhuwenda.marksix.util.DigestTools;

/**
 * Created by david on 15-9-23.
 */
public class PassWordUtil {


    public static String getEncodePwd(String password) {
        String encodePassWordFirst = DigestTools.md5String16(password);
        String base64PassWord = DigestTools.base64Encode(encodePassWordFirst.getBytes());
        return DigestTools.md5String(base64PassWord);
    }

    public static void main(String[] a){
        String ret = getEncodePwd("cck123");
        String ret2 = getEncodePwdWitTel("cck123","15920150976");
        System.out.println(ret);
        System.out.println(ret2);
    }

    public static String getEncodePwdWitTel(String password, String tel) {

        String encodePassWordFirst = DigestTools.md5String16(password);
        String base64PassWord = DigestTools.base64Encode(encodePassWordFirst.getBytes());
        return DigestTools.md5String(tel + base64PassWord);

    }
}
