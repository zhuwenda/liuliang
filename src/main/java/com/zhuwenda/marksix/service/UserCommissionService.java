package com.zhuwenda.marksix.service;


import com.zhuwenda.marksix.bean.ProductCommission;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.UserCommission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserCommissionService {

    private static final Logger logger = LoggerFactory.getLogger(UserCommissionService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProductCommissionService productCommissionService;
    @Autowired
    private WxuserService wxuserService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;


    public int generateOrderCommission(Order order){
        if (order == null){
            return 2;
        }

        String openid = order.getOpenid();
        String productid = order.getProductid();
        int orderPrice = order.getFeeAmount();
        String orderid = order.getOrderid();
        String sourceOpenid = order.getSourceOpenid();

        //产品佣金配置信息
        ProductCommission productCommission = productCommissionService.queryByProductid(productid);
        if(productCommission == null){//产品无佣金配置
            return 3;
        }

        String sql = "insert into t_user_commission(openid,grade,amount,orderid) values(?,?,?,?)";
        List<Object[]> batchUpdateParam = new ArrayList<>();

        if(wxuserService.isVip(openid)){
            logger.info("下单用户为VIP用户，不进行任何佣金生成");
            return 0;
        }

        if(order.getChannelType()  != null){
            logger.info("此订单为渠道订单，不进行佣金生成");
            return 0;
        }


        //------------- 来源佣金生成 ----------------
        Integer commissionOfSourceOpenid = productCommission.getSourceOpenid();
        if (sourceOpenid != null && !"".equals(sourceOpenid.trim()) && commissionOfSourceOpenid != null && commissionOfSourceOpenid > 0){
            Object[] objs = new Object[4];
            objs[0] = sourceOpenid;
            objs[1] = UserCommission.GRADE_SOURCE_OPENID;
            objs[2] = orderPrice * commissionOfSourceOpenid / 100;
            objs[3] = orderid;
            batchUpdateParam.add(objs);
            logger.info("将生成一条来源佣金记录");
        }
        //-------------------------

        //------------------ 推荐人佣金生成 ------------------
        if(batchUpdateParam.size() > 0){
            logger.info("来源统计佣金已生成，不再进行推荐人佣金生成");
        }else {
            String[] recommenders = wxuserService.queryRecommenderByOpenid(openid);
            if(recommenders != null && recommenders.length > 0){

                Integer[] level = new Integer[3];
                level[0] = productCommission.getLevel1();
                level[1] = productCommission.getLevel2();
                level[2] = productCommission.getLevel3();

                for (int i = 0; i < recommenders.length; i++){
                    if (recommenders[i] != null && level[i] != null){
                        Object[] objs = new Object[4];
                        objs[0] = recommenders[i];

                        if (i == 0){
                            objs[1] = UserCommission.GRADE_LEVEL1;
                        }else if(i == 1){
                            objs[1] = UserCommission.GRADE_LEVEL2;
                        }else if(i == 2){
                            objs[1] = UserCommission.GRADE_LEVEL3;
                        }else {//上线会员数量达到4级，异常！
                            logger.warn("上线会员数量达到4级，异常！");
                            objs[1] = "" + i;
                        }
                        //TODO 金额问题
                        objs[2] = orderPrice * level[i] / 100;
                        objs[3] = orderid;
                        batchUpdateParam.add(objs);

                        //发送模板消息
                        if((int)objs[2] > 0){
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            BigDecimal totalFee = new BigDecimal(orderPrice);
                            BigDecimal commissionAmount = new BigDecimal((int)objs[2]);
                            BigDecimal amount = totalFee.divide(new BigDecimal("100"));
                            BigDecimal commission = commissionAmount.divide(new BigDecimal("100"));
                            wxTemplateMsgService.getCommission(recommenders[i],orderid,decimalFormat.format(amount)+"元",decimalFormat.format(commission)+"元",new Date());
                        }else {
                            logger.warn("生成的佣金金额不大于0，不发送模板消息");
                        }
                    }
                }
            }
        }
        //------------------



        if (batchUpdateParam.size() > 0){
            int count = jdbcTemplate.batchUpdate(sql,batchUpdateParam).length;
            logger.info("订单{}生成了{}条佣金记录",orderid,count);
        }

        return 0;
    }

    public long queryUserCommissionByGrade(String openid, String grade){
        String sql = "select sum(t1.amount) from t_user_commission t1 where t1.openid=? and t1.grade=?";
        return jdbcTemplate.queryForLong(sql,new Object[]{openid,grade});
    }

    public long queryUserCommission(String openid){
        String sql = "select sum(t1.amount) from t_user_commission t1 where t1.openid=? and t1.grade in('level1','level2','level3')";
        return jdbcTemplate.queryForLong(sql,new Object[]{openid});
    }
}
