package com.zhuwenda.marksix.service;


import com.zhuwenda.marksix.bean.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ParameterService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Parameter queryByKey(String key){
        String sql = "select * from t_parameter where param_key = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{key}, new BeanPropertyRowMapper<Parameter>(Parameter.class));
    }

}
