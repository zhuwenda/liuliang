package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VipUserService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 将用户升级为vip用户
     * @param openid 用户的openid
     * @return
     */
    public int upgradeToVip(String openid){
        String sql = "insert into t_vip values(?)";
        int num = jdbcTemplate.update(sql,openid);
        return num == 1 ? 0 : 1;
    }

    /**
     * 是否存在此用户
     * @param openid
     * @return
     */
    public boolean isExist(String openid){
        if (openid == null || "".equals(openid.trim())){
            return false;
        }
        String sql = "select count(*) from t_vip where openid=?";
        long count = jdbcTemplate.queryForLong(sql, new Object[]{openid});
        return count == 1 ? true : false;
    }
}
