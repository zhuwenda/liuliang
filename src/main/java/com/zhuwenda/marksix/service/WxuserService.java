package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.Pager;
import com.zhuwenda.marksix.bean.PaginationSupport;
import com.zhuwenda.marksix.bean.UserCommission;
import com.zhuwenda.marksix.bean.Wxuser;
import com.zhuwenda.marksix.restfulservice.Vip;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WxuserService {

    private static final Logger logger = Logger.getLogger(WxuserService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private WithdrawService withdrawService;

    public boolean isVip(String openid){

        String sql = "select * from t_vip where openid=?";
        List<Vip> ret = jdbcTemplate.query(sql, new Object[]{openid}, new BeanPropertyRowMapper<Vip>(Vip.class));
        return ret.size() == 1 ? true : false;
    }

    /**
     * 添加用户
     * @param wxuser
     * @return 成功时返回 0
     */
    public int addUser(Wxuser wxuser){
        if(wxuser != null && wxuser.getSubscribeTime() == null){
            wxuser.setSubscribeTime(new Date());
        }
        NamedParameterJdbcTemplate namedparam = new NamedParameterJdbcTemplate(jdbcTemplate);
        String sql = "INSERT INTO `t_wxuser`" +
                " (`openid`, `nickname`, `headimgurl`, `subscribe`, `recommender`, `subscribe_time`)" +
                " VALUES (:openid, :nickname, :headimgurl, :subscribe, :recommender, :subscribeTime)";
        int num = namedparam.update(sql,new BeanPropertySqlParameterSource(wxuser));
        return num == 1 ? 0 : 1;
    }

    /**
     * 添加不存在的用户，实际上是先检查该openid的用户存不存在，不存在，则添加用户。存在，则返回2.
     * @param wxuser
     * @return 成功时返回0.失败：已存在返回2.失败：其他返回非0和2
     */
    public int addUserNotExist(Wxuser wxuser){
        if (isExist(wxuser.getOpenid())){
            return 2;
        }else {
            return addUser(wxuser);
        }
    }

    public int addOrUpdate(Wxuser wxuser){
        if (isExist(wxuser.getOpenid())){
            return updateUser(wxuser);
        }else {
            return addUser(wxuser);
        }
    }

    /**
     * 修改用户昵称和头像
     * @param wxuser
     * @return 成功时返回0
     */
    public int updateUser(Wxuser wxuser){
        String sql = "update t_wxuser set nickname=?,headimgurl = ? where openid = ?";
        int num = jdbcTemplate.update(sql, new Object[]{wxuser.getNickname(), wxuser.getHeadimgurl(), wxuser.getOpenid()});
        return num == 1 ? 0 : 1;
    }

    /**
     * 是否存在此用户
     * @param openid
     * @return
     */
    public boolean isExist(String openid){
        if (openid == null || "".equals(openid.trim())){
            return false;
        }
        String sql = "select count(*) from t_wxuser where openid=?";
        long count = jdbcTemplate.queryForLong(sql, new Object[]{openid});
        return count == 1 ? true : false;
    }

    public Wxuser queryUserByOpenid(String openid){
        String sql = "select * from t_wxuser where openid=?";
        try{
            return jdbcTemplate.queryForObject(sql, new Object[]{openid}, new BeanPropertyRowMapper<Wxuser>(Wxuser.class));
        }catch (Exception e){
            return null;
        }
    }

    public String[] queryRecommenderByOpenid(String openid){
        String sql = "select t2.openid as level1, t3.openid as level2, t4.openid as level3" +
                "        from t_wxuser t1" +
                "        LEFT JOIN t_wxuser t2 on t1.recommender = t2.openid" +
                "        LEFT JOIN t_wxuser t3 on t2.recommender = t3.openid" +
                "        LEFT JOIN t_wxuser t4 on t3.recommender = t4.openid" +
                "        where t1.openid=?";
        Map<String,Object> map  = jdbcTemplate.queryForMap(sql,openid);
        String[] recommenders = new String[3];
        if (map != null){
            recommenders[0] = (String)map.get("level1");
            recommenders[1] = (String)map.get("level2");
            recommenders[2] = (String)map.get("level3");
        }
        return recommenders;
    }

    public long queryCountOfLevel1(String openid){
        if (openid == null || "".equals(openid.trim())){
            return 0;
        }
        String sql = "select count(*) from t_wxuser t1 where t1.recommender = ?";
        return jdbcTemplate.queryForLong(sql,new Object[]{openid});
    }


    public long queryCountOfLevel2(String openid){
        if (openid == null || "".equals(openid.trim())){
            return 0;
        }
        String sql = "select count(*) from t_wxuser t1 where t1.recommender in " +
                "(select t2.openid from t_wxuser t2 where t2.recommender = ?)";
        return jdbcTemplate.queryForLong(sql,new Object[]{openid});
    }

    public long queryCountOfLevel3(String openid){
        if (openid == null || "".equals(openid.trim())){
            return 0;
        }
        String sql = "select count(*) from t_wxuser t1 where t1.recommender in " +
                "(select t2.openid from t_wxuser t2 where t2.recommender in " +
                "(select t3.openid from t_wxuser t3 where t3.recommender = ?) )";
        return jdbcTemplate.queryForLong(sql,new Object[]{openid});
    }


    public PaginationSupport queryListOfLevel1(String openid,int page,int pageSize){
        if (openid == null || "".equals(openid.trim())){
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,0));
        }
        if (page<=0) {
            page=1;
        }
        if (pageSize<=0) {
            pageSize=Pager.DEFAULT_PAGESIZE;
        }
        int firstResult=(page-1)*pageSize;
        int maxResult=pageSize;
        long count = queryCountOfLevel1(openid);
        if(count > 0){
            String sql = "select * from t_wxuser t1 where t1.recommender = ? order by t1.subscribe_time desc limit ?,?";
            List<Wxuser> list = jdbcTemplate.query(sql, new Object[]{openid,firstResult,maxResult}, new BeanPropertyRowMapper<Wxuser>(Wxuser.class));
            List<Map<String,Object>> listRet = new ArrayList<>();
            String sqlAmount = "select sum(t2.amount) from t_order t1 inner join t_user_commission t2 on t1.orderid=t2.orderid " +
                    " where t1.openid=? and t2.openid=? and t2.grade=?";
            list.stream().forEach(wxuser->{
                long sum = jdbcTemplate.queryForLong(sqlAmount,wxuser.getOpenid(),openid, UserCommission.GRADE_LEVEL1);
                Map<String,Object> map = new HashMap<String, Object>();
                map.put("wxuser",wxuser);
                map.put("sum",sum);
                listRet.add(map);
            });
            return new PaginationSupport(listRet,new Pager(page,pageSize,count));
        }else {
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,count));
        }
    }


    public PaginationSupport queryListOfLevel2(String openid,int page,int pageSize){
        if (openid == null || "".equals(openid.trim())){
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,0));
        }
        if (page<=0) {
            page=1;
        }
        if (pageSize<=0) {
            pageSize=Pager.DEFAULT_PAGESIZE;
        }
        int firstResult=(page-1)*pageSize;
        int maxResult=pageSize;
        long count = queryCountOfLevel2(openid);
        if(count > 0){

            String sql = "select t1.* from t_wxuser t1 where t1.recommender in " +
                    "(select t2.openid from t_wxuser t2 where t2.recommender = ?) order by t1.subscribe_time desc limit ?,?";
            List<Wxuser> list = jdbcTemplate.query(sql, new Object[]{openid,firstResult,maxResult}, new BeanPropertyRowMapper<Wxuser>(Wxuser.class));
            List<Map<String,Object>> listRet = new ArrayList<>();
            String sqlAmount = "select sum(t2.amount) from t_order t1 inner join t_user_commission t2 on t1.orderid=t2.orderid " +
                    " where t1.openid=? and t2.openid=? and t2.grade=?";
            list.stream().forEach(wxuser->{
                long sum = jdbcTemplate.queryForLong(sqlAmount,wxuser.getOpenid(),openid, UserCommission.GRADE_LEVEL2);
                Map<String,Object> map = new HashMap<String, Object>();
                map.put("wxuser",wxuser);
                map.put("sum",sum);
                listRet.add(map);
            });
            return new PaginationSupport(listRet,new Pager(page,pageSize,count));
        }else {
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,count));
        }
    }

    public PaginationSupport queryListOfLevel3(String openid,int page,int pageSize){
        if (openid == null || "".equals(openid.trim())){
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,0));
        }
        if (page<=0) {
            page=1;
        }
        if (pageSize<=0) {
            pageSize=Pager.DEFAULT_PAGESIZE;
        }
        int firstResult=(page-1)*pageSize;
        int maxResult=pageSize;
        long count = queryCountOfLevel3(openid);
        if(count > 0){

            String sql = "select t1.* from t_wxuser t1 where t1.recommender in " +
                    "(select t2.openid from t_wxuser t2 where t2.recommender in " +
                    "(select t3.openid from t_wxuser t3 where t3.recommender = ?) ) order by t1.subscribe_time desc limit ?,?";
            List<Wxuser> list = jdbcTemplate.query(sql, new Object[]{openid,firstResult,maxResult}, new BeanPropertyRowMapper<Wxuser>(Wxuser.class));
            List<Map<String,Object>> listRet = new ArrayList<>();
            String sqlAmount = "select sum(t2.amount) from t_order t1 inner join t_user_commission t2 on t1.orderid=t2.orderid " +
                    " where t1.openid=? and t2.openid=? and t2.grade=?";
            list.stream().forEach(wxuser->{
                long sum = jdbcTemplate.queryForLong(sqlAmount,wxuser.getOpenid(),openid, UserCommission.GRADE_LEVEL3);
                Map<String,Object> map = new HashMap<String, Object>();
                map.put("wxuser",wxuser);
                map.put("sum",sum);
                listRet.add(map);
            });
            return new PaginationSupport(listRet,new Pager(page,pageSize,count));
        }else {
            return new PaginationSupport(new ArrayList<>(),new Pager(0,0,count));
        }
    }



    public long queryUsableBalance(String openid){
        long userCommission = userCommissionService.queryUserCommission(openid);
        long frozenBalance = withdrawService.queryUserFrozenBalance(openid);
        if(userCommission < frozenBalance){
            String errmsg = "严重，用户 "+ openid + " 的提成总额 小于 提现中和提现成功总额";
            logger.error(errmsg);
            throw new IllegalStateException(errmsg);
        }
        return userCommission - frozenBalance;
    }

}
