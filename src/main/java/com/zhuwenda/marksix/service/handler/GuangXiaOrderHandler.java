package com.zhuwenda.marksix.service.handler;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.VipUserService;
import com.zhuwenda.marksix.service.WxService;
import com.zhuwenda.marksix.service.WxTemplateMsgService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class GuangXiaOrderHandler implements IOrderPaySuccessHandler{

    private static final Logger logger = LoggerFactory.getLogger(GuangXiaOrderHandler.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private WxService wxService;
    @Autowired
    private VipUserService vipUserService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;


    @Override
    public void handle(Order order, Product product, Map<String, Object> data) {
        try {

            orderService.markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_SUCCESS);
            if(vipUserService.isExist(order.getOpenid())){
                logger.info("用户已经是vip用户，不需升级为vip用户");
            }else {
                vipUserService.upgradeToVip(order.getOpenid());
                logger.info("用户升级为vip用户成功，openid:{}",order.getOpenid());
                WxMpUser wxMpUser = wxService.userInfo(order.getOpenid(), null);
                String nickName = StringUtils.hasText(wxMpUser.getNickname()) ? wxMpUser.getNickname() : "用户";
                wxTemplateMsgService.beVip(order.getOpenid(),nickName,"zsllpt111068");
            }

        }catch (Exception e){
            orderService.updateOuterOrderInfo(order.getOrderid(), null, "本地异常:"+e.getMessage());
            logger.error("用户升级为vip用户时发生异常",e);
        }


    }

    @Override
    public boolean shouldHandle(Product product) {
        if(product != null && "广夏".equals(product.getInterfaceFlag())){
            logger.info("匹配到广夏产品");
            return true;
        }
        return false;
    }
}
