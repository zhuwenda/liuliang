package com.zhuwenda.marksix.service.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.hotdata.HotDataResult;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.util.DigestTools;
import me.chanjar.weixin.common.util.http.Utf8ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class HotdataOrderHandler implements IOrderPaySuccessHandler{

    private static final Logger logger = LoggerFactory.getLogger(HotdataOrderHandler.class);


    @Value("${liuliang.hotdata.api}")
    private String api;
    @Value("${liuliang.hotdata.openid}")
    private String openid;
    @Value("${liuliang.hotdata.secret}")
    private String secret;
    @Value("${liuliang.hotdata.appkey}")
    private String appkey;
    @Value("${liuliang.hotdata.notifyUrl}")
    private String notifyUrl;

    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;


    @Override
    public void handle(Order order, Product product, Map<String, Object> data) {
        try {


            Map<String,Object> dataMap = new TreeMap<String,Object>();
            dataMap.put("method", "FlowRechargePPS");
            dataMap.put("openid", openid);
            dataMap.put("appkey", appkey);
            dataMap.put("mobile", order.getTopupMobile());
            dataMap.put("notify_url", URLEncoder.encode(notifyUrl));
            dataMap.put("product_id", product.getOuterProductid());
            dataMap.put("product_value", product.getOuterExtend1());
            dataMap.put("timestamp", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            System.out.println("请求参数:");
            dataMap.entrySet().stream().forEach(System.out::println);
            String str = dataMap.entrySet().stream()
                    .sorted((a,b)-> a.getKey().compareTo(b.getKey()))
                    .map(a->a.toString())
                    .reduce("", (a, b) -> a.concat("&").concat(b))
                    ;
            String concatParamString = str.substring(1, str.length());
            logger.debug("升序排序并用&连接：" + concatParamString);
            String concatSecretString = concatParamString + secret;
            logger.debug("拼接双方约定的密钥：" + concatSecretString);
            String lowerString = concatSecretString.toLowerCase();
            logger.debug("转化为小写:" + lowerString);
            String sign = DigestTools.md5String(lowerString);
            logger.debug("计算签名字符串：" + sign);

            dataMap.put("sign",sign);

            ObjectMapper ob = new ObjectMapper();
            String postData = ob.writeValueAsString(dataMap);

            HttpPost httpPost = new HttpPost(api);

            List<BasicNameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("data",postData));
            ;
            System.out.println("请求实体：");
            System.out.println(dataMap);
            httpPost.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));

            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpPost);
            String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
            logger.info("响应报文：");
            logger.info(responseContent);
            ObjectMapper objectMapper = new ObjectMapper();
            HotDataResult result = objectMapper.readValue(responseContent, HotDataResult.class);
            if("0".equals(result.getCode())){
                orderService.markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_TOPUPING, result.getData().getTransactionId());
                logger.info("hotdata平台提交充值成功");
            }
            else if ("403".equals(result.getCode()) || "404".equals(result.getCode())){
                /*
                403
                套餐不存在
                404
                账号余额不足
                */
                orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.PRODUCT_OR_ACCOUNT_EXCEPTION);
            }
            else{
                orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.NORMAL);
                logger.error("充值失败："+result.getMessage());
            }

        }catch (Exception e){
            orderService.updateOuterOrderInfo(order.getOrderid(), null, "本地异常:"+e.getMessage());
            logger.error("调用hotdata充值流量接口时发生异常",e);
        }


    }

    @Override
    public boolean shouldHandle(Product product) {
        if(product != null && "hotdata".equals(product.getInterfaceFlag())){
            logger.info("匹配到hotdata流量平台产品");
            return true;
        }
        return false;
    }
}
