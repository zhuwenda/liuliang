package com.zhuwenda.marksix.service.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.jingnan.JingNanChargeResult;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.util.DigestTools;
import me.chanjar.weixin.common.util.http.Utf8ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.Map;

@Service
public class JingNanOrderHandler implements IOrderPaySuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(JingNanOrderHandler.class);

    @Value("${liuliang.jingnan.api}")
    private String api;
    @Value("${liuliang.jingnan.account}")
    private String account;
    @Value("${liuliang.jingnan.key}")
    private String key;
    @Value("${liuliang.jingnan.notifyUrl}")
    private String notifyUrl;


    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @Override
    public void handle(Order order, Product product, Map<String, Object> data) {
        logger.info("准备提交流量充值请求，订单号：{}",order.getOrderid());
        String productid = product.getOuterProductid();
        String mobile = order.getTopupMobile();
        JingNanChargeResult result = charge(mobile,productid);
        if("000".equals(result.getCode())){
            logger.info("流量充值提交成功");
            orderService.markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_TOPUPING, result.getTaskid());
            logger.info("标记订单状态为充值中成功");
        }
        else if ("002".equals(result.getCode()) || "005".equals(result.getCode())){
            /*
            002
            余额不足
            005
            不存在指定流量包
             */
            orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.PRODUCT_OR_ACCOUNT_EXCEPTION);
        }
        else {
            logger.warn("流量充值提交失败，错误代码：{},原因：{}", result.getCode(), result.getMessage());
            orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.NORMAL);
        }
    }

    @Override
    public boolean shouldHandle(Product product) {
        if(product != null && "京南".equals(product.getInterfaceFlag())){
            logger.info("匹配到京南流量平台产品,内部产品编号：{}",product.getProductid());
            return true;
        }
        return false;
    }

    public JingNanChargeResult charge(String mobile,String productid){
        try{
            logger.info("调用京南流量充值接口");
            String url = api + "?action=charge&v=1.0&account="+account+"&mobile="+mobile+"&id="+productid+"&notifyurl="+ URLEncoder.encode(notifyUrl);
            String toBeMd5 = "account="+account+"&id="+productid+"&mobile="+mobile+"&notifyurl="+notifyUrl+"&key="+key;

            url += "&sign="+ DigestTools.md5String(toBeMd5.toLowerCase()).toUpperCase();
            logger.info("请求URL：{}",url);
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpGet);
            String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
            logger.info("响应报文：{}",responseContent);
            ObjectMapper objectMapper = new ObjectMapper();
            JingNanChargeResult result = objectMapper.readValue(responseContent,JingNanChargeResult.class);

            return result;
        }catch (Exception e){
            logger.error("京南流量充值异常，手机号:{},京南产品id:{}",mobile,productid,e);
            return null;
        }
    }
}
