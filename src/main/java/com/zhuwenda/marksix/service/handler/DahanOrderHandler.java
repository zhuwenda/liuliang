package com.zhuwenda.marksix.service.handler;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.dahan.DaHanProductCarrierEnum;
import com.zhuwenda.marksix.bean.dahan.DaHanResult;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.service.RechargeService;
import com.zhuwenda.marksix.service.UserCommissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DahanOrderHandler implements IOrderPaySuccessHandler{

    private static final Logger logger = LoggerFactory.getLogger(DahanOrderHandler.class);


    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private ProductService productService;


    @Override
    public void handle(Order order, Product product, Map<String, Object> data) {
        try{
            logger.info("即将调用大汉流量平台充值接口,orderid:"+order.getOrderid());
            if(order.getNum() <= 20){

                DaHanProductCarrierEnum productCarrierEnum = null;
                if(product.getCarrier() == 1){
                    productCarrierEnum = DaHanProductCarrierEnum.DAHAN_CMCC;
                }else if(product.getCarrier() == 2){
                    productCarrierEnum = DaHanProductCarrierEnum.DAHAN_CUCC;
                }else if(product.getCarrier() == 3){
                    productCarrierEnum = DaHanProductCarrierEnum.DAHAN_CTCC;
                }else {
                    logger.error("没有配置运营商");
                    return ;
                }
                DaHanResult result = rechargeService.createOrder(order.getTopupMobile(), order.getOrderid(),productCarrierEnum,product.getOuterProductid(),product.getOuterExtend1());

                if("00".equals(result.getResultCode())){
                    logger.info("流量充值提交成功");
                    orderService.markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_TOPUPING,result.getClientOrderId());
                    logger.info("标记充值状态为 充值中 成功");
                }
                else if ("03".equals(result.getResultCode()) || "09".equals(result.getResultCode())){
                    /*
                    03：无包大小和产品 ID
                    09：余额不足
                    */
                    orderService.orderFailedNotification(order, product, result.getResultMsg(), TopupFailType.PRODUCT_OR_ACCOUNT_EXCEPTION);
                }
                else {
                    orderService.orderFailedNotification(order, product, result.getResultMsg(), TopupFailType.NORMAL);
                    logger.error("充值失败");
                }
            }
        }catch (Exception e){
            orderService.updateOuterOrderInfo(order.getOrderid(),null,"本地异常:"+e.getMessage());
            logger.error("调用大汉流量接口时发生一个异常",e);
        }
    }

    @Override
    public boolean shouldHandle(Product product) {
        if(product != null && "大汉".equals(product.getInterfaceFlag())){
            logger.info("匹配到大汉流量平台产品");
            return true;
        }
        return false;
    }
}
