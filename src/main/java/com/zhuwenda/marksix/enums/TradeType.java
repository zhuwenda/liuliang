package com.zhuwenda.marksix.enums;

/**
 * 交易类型：人工充值、系统自动充值
 */
public enum TradeType {
    /** 人工充值 */
    MANUAL,
    /** 系统自动充值 */
    SYSTEM_AUTO
}
