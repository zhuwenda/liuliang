package com;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.dahan.DaHanCallback;
import com.zhuwenda.marksix.bean.hotdata.CallbackData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:applicationContext.xml","classpath:springmvc-servlet.xml"})
public class ControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;


    @Before
    public void before(){
        mockMvc = webAppContextSetup(this.wac).build();
    }


    @Test
    public void testHotDataCallback() throws Exception{
        CallbackData callbackData = new CallbackData();
        callbackData.setTransactionid("23546576878089654658769987");
        callbackData.setMobile("13012345678");
        callbackData.setStatus("1");
        callbackData.setProductid("1001");
        callbackData.setMessage("充值成功");

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonData = objectMapper.writeValueAsString(callbackData);
        System.out.println(jsonData);

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/hotdata/callback")
                        .param("myname","myvalue")
                        //.content(jsonData)
                        //.header("Content-Type", "application/json")
                        .header("Accept", "*/*")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6"))

                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void testDaHanCallback() throws Exception{

        DaHanCallback daHanCallback = new DaHanCallback();
        daHanCallback.setClientOrderId("23453532");
        daHanCallback.setStatus(0);
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonData = objectMapper.writeValueAsString(daHanCallback);
        System.out.println(jsonData);

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/dahan/callback")
                                .content(jsonData)
                                .header("Content-Type", "application/json")
                        .header("Accept", "*/*")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6"))

                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

}
