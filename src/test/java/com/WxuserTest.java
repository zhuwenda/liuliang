package com;


import com.zhuwenda.marksix.bean.PaginationSupport;
import com.zhuwenda.marksix.bean.Wxuser;
import com.zhuwenda.marksix.service.WxService;
import com.zhuwenda.marksix.service.WxuserService;
import com.zhuwenda.marksix.util.EmojiFilter;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class WxuserTest {

    @Autowired
    private WxService wxService;
    @Autowired
    private WxuserService wxuserService;


    @Test
    public void testWxuserIsExist(){
        Assert.assertFalse(wxuserService.isExist("hgajgljksdglkjafskljfkjakdjfaslkdfkjask"));
    }

    @Test
    public void testEmoji(){
        String text = "\uD83D\uDC4C不够高很久很\uD83D\uDE04";
        System.out.println(text.replaceAll("[\\ud83c\\udc00-\\ud83c\\udfff]|[\\ud83d\\udc00-\\ud83d\\udfff]|[\\u2600-\\u27ff]", "*"));
    }

    @Test
    public void testInit() throws Exception{

        System.out.println("-------------*****************-------------");

        WxMpUserList list = wxService.userList(null);
        //TODO nextOpenid
        List<String> openids = list.getOpenIds();
        if(openids != null && openids.size() > 0){
            List<WxMpUser> listTemp = wxService.userinfoBatchget(openids);
            if(listTemp != null && listTemp.size() > 0){
                for(int j= 0; j < listTemp.size(); j++){
                    WxMpUser wxMpUser = listTemp.get(j);
                    Wxuser wxuser = new Wxuser();
                    wxuser.setOpenid(wxMpUser.getOpenId());
                    wxuser.setHeadimgurl(wxMpUser.getHeadImgUrl());
                    wxuser.setNickname(EmojiFilter.filterEmoji(wxMpUser.getNickname()));
                    wxuserService.addOrUpdate(wxuser);
                }
            }
        }

        System.out.println("---------------------------------------");
    }

    @Test
    public void testWxuserAdd(){
        Wxuser wxuser = new Wxuser();
        wxuser.setHeadimgurl("http://www.baidu.com/a=242432");
        wxuser.setNickname("nickname");
        wxuser.setOpenid("vxv");
        wxuser.setRecommender("sdkflkjsdjzk fsfsmfsfjkskdfksjdf");
        wxuser.setSubscribe(1);
        wxuser.setSubscribeTime(new Date(2016-1900,0,1,1,1,1));
        Assert.assertEquals(0, wxuserService.addUserNotExist(wxuser));
    }

    @Test
    public void testWxuserUpdate(){
        Wxuser wxuser = new Wxuser();
        wxuser.setOpenid("hgajgljksdglkjafskljfkfjakdjfaslkdfkjask");
        wxuser.setNickname("昵称");
        Assert.assertEquals(0, wxuserService.updateUser(wxuser));
    }


    @Test
    public void testWxuserQueryByOpenid(){
        Assert.assertNotNull(wxuserService.queryUserByOpenid("hgajglddjkljfkfjakdjfaslkdfkjask"));
    }

    @Test
    public void testWxuserQueryRecommender(){
        String[] ret = wxuserService.queryRecommenderByOpenid("4");
        Assert.assertArrayEquals(new String[]{"3", "2", null}, ret);

    }

    @Test
    public void testQueryCountOfLevel(){
        String openid = "2";
        long l1 = wxuserService.queryCountOfLevel1(openid);
        long l2 = wxuserService.queryCountOfLevel2(openid);
        long l3 = wxuserService.queryCountOfLevel3(openid);
        System.out.println("----------");
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println("----------");
    }

    @Test
    public void testQueryOfLevel(){
        String openid = "ojPcIuNNMNsErIG8t1_Mn8bqi2fw";
        PaginationSupport ps1 = wxuserService.queryListOfLevel1(openid,1,3);
        PaginationSupport ps2 = wxuserService.queryListOfLevel2(openid, 1, 3);
        PaginationSupport ps3 = wxuserService.queryListOfLevel3(openid, 1, 3);
        System.out.println("----------");
        System.out.println(ps1);
        System.out.println(ps2);
        System.out.println(ps3);
        System.out.println("----------");
    }

    @Test
    public void testUsableBalance(){
        System.out.println("---------");
        System.out.println(wxuserService.queryUsableBalance("2"));
    }
}
