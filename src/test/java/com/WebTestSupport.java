package com;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.dahan.DaHanCallback;
import com.zhuwenda.marksix.bean.hotdata.CallbackData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:applicationContext.xml","classpath:springmvc-servlet.xml"})
public class WebTestSupport {

    @Autowired
    private WebApplicationContext wac;

    protected MockMvc mockMvc;


    @Before
    public void before(){
        mockMvc = webAppContextSetup(this.wac).build();
    }

    public MockMvc getMockMvc() {
        return mockMvc;
    }
}
