package com;


import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.CacheManagerBuilder;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.CacheConfigurationBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.expiry.Expiry;
import org.junit.Test;

public class EhcacheTest {

    @Test
    public void test1  (){
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build(true);
        CacheConfiguration cacheConfiguration = null;
        Expiry expiry = Expirations.noExpiration();
        cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder().withExpiry(expiry).buildConfig(String.class, Object.class);
        Cache<String,Object> cache = cacheManager.createCache("cache1", cacheConfiguration);
        cache.put("key1", 1);


        for(int i = 0; i < 10 ; i++){


            CacheConfiguration temp = cacheManager.getRuntimeConfiguration().getCacheConfigurations().get("cache1");
            Duration duration = temp.getExpiry().getExpiryForAccess("key1", 1);
            System.out.println(duration.getAmount()+" "+duration.getTimeUnit());

            Integer value = (Integer)cache.get("key1");
            System.out.println(value);
            if(value != null){
                value++;
            }
            cache.put("key1",value);
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
